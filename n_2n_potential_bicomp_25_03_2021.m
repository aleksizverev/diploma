clear;
format long;

% КОНСТАНТЫ
rounds = 1;
eps = 0.001;
M = 1836/2;
dx = 0.05;
energy = -0.16;
energy_res_arr = [];
u_0 = 0.1745593;

 figure;
 hold on;
 grid on;
 xlabel('x');
 ylabel('Относительные единицы');
 
for k = 1:1:rounds
net = auto_mesh(dx);
N = length(net)-2;
d = randi(N, 1, N);
B = zeros(N, N);

% составление массивов шагов и потенциала
[h, u] = count_h(net, N);

% составление матрицы B
for n = 1:1:N
    if n == 1
        B(n, n) = (h(n) + h(n+1));
        B(n, n+1) = h(n+1);
    end
    if n == N
        B(n, n) = (h(n) + h(n+1));
        B(n, n-1) = h(n);
    end
    if n > 1 && n < N
        B(n, n) = (h(n) + h(n+1));
        B(n, n-1) = h(n);
        B(n, n+1) = h(n+1);
    end
end
B = B*M;
 [x, energy_corrected] = bicomp(h, u, N, energy, M, d, eps, B);
 energy_res_arr(k) = energy_corrected + u_0
 
 x_axis = net;
 x_axis(1) = [];
 x_axis(length(x_axis)) = [];
 %title("График решения");
 plot(x_axis, abs(x), '-k', 'MarkerSize', 8,'LineWidth', 4 );
 
% сгущение
 dx = dx/2;
end

figure;
grid on;
hold on;
title("График потенциала")
plot(net ,u, '--g.','MarkerSize', 10, 'LineWidth', 4, 'MarkerEdgeColor','b');


% апостериорная оценка(построенная по разностям)
diff = [];
diff_relat = [];
for k = 1:1:rounds-1
    diff(k) = log10(abs(energy_res_arr(k+1) - energy_res_arr(k))/3)
end

diff_relat = 10^(diff(end))/energy_res_arr(end) * 100

figure;
hold on;
grid on;
title('Сходимость решения, бикомпактная схема, квазиравномерная сетка, потенциал "N-2N"')
plot(diff, '--g+', 'MarkerSize', 30, 'LineWidth', 4, 'MarkerEdgeColor','b');

% linspace(2, 8, 7)


function [x,  energy_corrected] = bicomp(h, u, N, energy, M, d, eps, B)

% первая итерация
[a, b, c] = coeffs(h, u, N, energy, M);

x = progonka(a, b, c, d, N);
x_supp = x * B;
energy_corrected = energy + sum(x_supp .* d)/sum(x_supp .* x_supp);
x = x/(x*x')^0.5;

nn = 1;

% зацикливание 
while abs((energy_corrected - energy)/(energy_corrected+energy)) > eps
    
    condition = (energy_corrected - energy)/(energy_corrected+energy);
    
    energy = energy_corrected;
    
    x_prev = x;
    d = x_prev * B;
    [a, b, c] = coeffs(h, u, N, energy, M);
    
    x = progonka(a, b, c, d, N);
    x_supp = x * B;
    energy_corrected = energy + sum(x .* x_prev)/sum(x .* x);
    tmp1 = sum(x .* x);
    tmp2 = sum(x.*x_prev);
    x = x/sum(x.*x)^0.5;
    nn = nn + 1;
    
end
nn
end

function net = auto_mesh(dx)
% введите границы областей
net = -1:dx:7;
% net = 7* ksi./(1-ksi.^2);
end

function [h, u] = count_h(net, N)
h = [];
u = [];

for i = 1:1:N+1
    h(i) = abs(net(i+1) - net(i));
end
 
for i = 1:1:N+2
%     потенциал вида n-2n
     n = 6;
     a = 0.9864369660382; % а.е.д.
     b = -4.5170120305; % а.е.д
     u_0 = 0.1745593; % эн. Хартри
     u(i) = u_0 * ( (n*a/(net(i)-b))^(2*n) - 2* (n*a/(net(i)-b))^n );
end
end

function [a, b, c] = coeffs(h, u, N, energy, M)
a = zeros(1, N);
b = zeros(1, N);
c = zeros(1, N);

for n = 2:1:N+1
    c(n-1) = (2/h(n) + 2/h(n-1) + M* h(n) * u(n) + M* h(n-1) * u(n-1)  - M* energy * (h(n) + h(n-1)));
end

for n = 2:1:N
    b(n-1) = (2/h(n) - M* h(n) * u(n) + M* energy* h(n));
end

for n = 2:1:N
    a(n) = (2/h(n) - M* h(n) * u(n) + M* energy* h(n));
end

end

function x = progonka(a, b, c, d, N)

x = zeros(1, N);
alpha = zeros(1, N+1);
betha = zeros(1, N+1);

for n = 1:1:N
    alpha(n+1) = b(n)/(c(n)-a(n)*alpha(n));
    betha(n+1) = (a(n)*betha(n) + d(n))/(c(n) - a(n)*alpha(n));
end

x(N) = betha(N+1);
for n = N-1:-1:1
    x(n) = x(n+1)*alpha(n+1) + betha(n+1);
end

end
