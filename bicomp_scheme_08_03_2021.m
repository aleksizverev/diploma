clear;

% КОНСТАНТЫ
rounds = 1;
eps = 0.0001;
M = 1;
energy = 1;
v = 35;
N_ex = 80;
N_in = 320;
energy_res_arr = [];
steps = []

 figure;
 hold on;
 grid on;
 xlabel('x');
 ylabel('Относительные единицы');

for k = 1:1:rounds
net = auto_mesh(N_ex, N_in);
N = length(net)-2;
d = randi(N, 1, N);
B = zeros(N, N);

% составление массивов шагов и потенциала
[h, u] = count_h(net, N, v, N_ex, N_in);

% составление матрицы B
for n = 1:1:N
    if n == 1
        B(n, n) = (h(n) + h(n+1));
        B(n, n+1) = h(n+1);
    end
    if n == N
        B(n, n) = (h(n) + h(n+1));
        B(n, n-1) = h(n);
    end
    if n > 1 && n < N
        B(n, n) = (h(n) + h(n+1));
        B(n, n-1) = h(n);
        B(n, n+1) = h(n+1);
    end
end

 [x, energy_corrected] = bicomp(h, u, N, energy, M, d, eps, B, net);
 energy_res_arr(k) = energy_corrected;
 
 x_axis = net;
 x_axis(1) = [];
 x_axis(length(x_axis)) = [];
 plot(x_axis, x, '-k', 'MarkerSize', 8, 'LineWidth', 4);
 steps(k) = log10(N_ex + N_in);
 
% сгущение
 N_ex = N_ex*2;
 N_in = N_in*2;
end

% апостериорная оценка(построенная по разностям)
steps(1) = [];
diff = [];
for k = 1:1:rounds-1
    diff(k) = log10(abs(energy_res_arr(k+1) - energy_res_arr(k))/3);
end

figure;
hold on;
grid on;
xlabel('lg(N)');
ylabel('lg(E)');
%title('Погрешности относительно точного решения и сгущения сеток')
% plot(linspace(2, 7, 6) ,diff, '--g+', 'MarkerSize', 30, 'LineWidth', 4, 'MarkerEdgeColor','b');
plot(steps, diff, '-k^', 'MarkerSize', 13, 'LineWidth', 2, 'MarkerEdgeColor','k', 'MarkerFaceColor' ,[0 0 0]);

% linspace(2, 8, 7)

% "точная" оценка
% E = sqrt(2)*4.5;
E = dihot(8, 9, 0.0000001, v, 2);
diff_accurate =[];
for k = 1:1:rounds-1
    diff_accurate(k) = log10(abs(energy_res_arr(k+1) - E));
end

plot(steps, diff_accurate, '-k^', 'MarkerSize', 25, 'LineWidth', 2, 'MarkerEdgeColor','k');

three_dot_diff = [-0.248085224556268  -0.617570437167686  -0.957319164552606  -1.275581182464647  -1.584524719597449  -1.889337534739877]
plot(steps, three_dot_diff, '-k.', 'MarkerSize', 40, 'LineWidth', 2, 'MarkerEdgeColor','k');

three_dot_diff_accurate = [ -0.340716456463187  -0.667348028294929  -0.979725860292538  -1.285992930600023  -1.589535586630872  -1.891797112229640]
plot(steps, three_dot_diff_accurate, '-ko', 'MarkerSize', 25, 'LineWidth', 2, 'MarkerEdgeColor','k');
% lgd = legend('бикомпактная, по сгущению сеток', 'отн-но точного решения', 'трехточечная, по сгущению сеток', 'отн-но точного решения')

function [x,  energy_corrected] = bicomp(h, u, N, energy, M, d, eps, B, net)

[a, b, c] = coeffs(h, u, N, energy, M);

x = progonka(a, b, c, d, N);
x_supp = x * B;
energy_corrected = energy + sum(x_supp .* d)/sum(x_supp .* x_supp);
x = x/max(abs(x));

while abs((energy_corrected - energy)/(energy_corrected+energy)) > eps
    
    energy = energy_corrected;
    
    d = x * B;
    [a, b, c] = coeffs(h, u, N, energy, M);
    
    x = progonka(a, b, c, d, N);
    x_supp = x * B;
    energy_corrected = energy + sum(x_supp .* d)/sum(x_supp .* x_supp);
    x = x/max(abs(x));
    
end
end

function net = auto_mesh(N_ex, N_in)

% введите границы областей
l_ex_border = -10;
r_ex_border = 10;

l_in_border = -1;
r_in_border = 1;

% подсчет шага в центральной части в центральной части
step = (r_in_border - l_in_border)/N_in;

% рассчет внутренней вспомогательной сетки
in_net = [];
for n = 1:1:N_in-1
    in_net(n) = l_in_border + n*step;
end

% неравномерная сетка
net = [linspace(l_ex_border, l_in_border, N_ex), in_net, linspace(r_in_border, r_ex_border, N_ex)];

% равномерная сетка
% h1 = abs((l_ex_border- l_in_border))/N_ex;
% h2 = abs((l_in_border - r_in_border))/N_in;
% h3 = abs((r_in_border - r_ex_border))/N_ex;
% 
% net1 = l_ex_border:h1:l_in_border;
% net2 = l_in_border + h2 : h2 : r_in_border;
% net3 = r_in_border + h3 : h3 : r_ex_border;
% 
% net = [net1, net2, net3];

% -------------------------------------------------------------------------------
end

function [h, u] = count_h(net, N, v, N_ex, N_in)
h = [];
u = [];

for n = 1:1:N+1
    h(n) = abs(net(n+1) - net(n));

%     потенциальная яма конечной глубины
    if n <= (N_ex-1) | n >= (N_ex+N_in)
        u(n) = v;
    end
    if n >  (N_ex-1) && n < (N_ex+N_in)
        u(n) = 0;
    end

%     потенциал вида x^2
%     u(n) = net(n)^2;
end

end

% расчет коэффициентов прогонки
function [a, b, c] = coeffs(h, u, N, energy, M)
    a = zeros(1, N);
    b = zeros(1, N);
    c = zeros(1, N);

    for n = 2:1:N+1
        c(n-1) = (2/h(n) + 2/h(n-1) + M* h(n) * u(n) + M* h(n-1) * u(n-1) - M* energy * (h(n) + h(n-1)));
    end

    for n = 2:1:N
        b(n-1) = (2/h(n) - M* h(n) * u(n) + M* energy* h(n));
    end

    for n = 2:1:N
        a(n) = (2/h(n) - M* h(n) * u(n) + M* energy* h(n));
    end
end

% метод прогонки
function x = progonka(a, b, c, d, N)

x = zeros(1, N);
alpha = zeros(1, N+1);
betha = zeros(1, N+1);

for n = 1:1:N
    alpha(n+1) = b(n)/(c(n)-a(n)*alpha(n));
    betha(n+1) = (a(n)*betha(n) + d(n))/(c(n) - a(n)*alpha(n));
end

x(N) = betha(N+1);
for n = N-1:-1:1
    x(n) = x(n+1)*alpha(n+1) + betha(n+1);
end

end
