clear;

% КОНСТАНТЫ
rounds =7;
eps = 0.0001;
energy = 12;
v = 35;
N_ex = 40;             
N_in = 160;
energy_res_arr = [];
steps = []; % логирфм числа шагов для построения графика разностей СЗ

 figure;
 hold on;

for k = 1:1:rounds
net = auto_mesh(N_ex, N_in);
N = length(net)-2;
d = randi(N, 1, N);

% составление массивов шагов и потенциала
[h, u] = count_h(net, N, v, N_ex, N_in);
 
steps(k) = log(length(h));
 [x, energy_corrected] = dot_scheme(net, u, N, energy, d, eps);
 energy_res_arr(k) = energy_corrected;
 
 x_axis = net;
 x_axis(1) = [];
 x_axis(length(x_axis)) = [];
 plot(x_axis, x, '-o', 'MarkerSize', 8);
 
% сгущение
 N_ex = N_ex*2;
 N_in = N_in*2;
end


% апостериорная оценка(построенная по разностям)
diff = [];
for k = 1:1:rounds-1
    diff(k) = log10(abs(energy_res_arr(k+1) - energy_res_arr(k)));
end

figure;
hold on;
grid on;
title('График сходимости решения при трехточечной схеме на неравномерной сетке')
plot(diff, '--g+', 'MarkerSize', 30, 'LineWidth', 4, 'MarkerEdgeColor','b');

% "точная" оценка
% E = 9;

E = dihot(8, 9, 0.0000001, v, 2);
diff_accurate =[];
for k = 1:1:rounds-1
    diff_accurate(k) = log10(abs(energy_res_arr(k+1) - E));
end
 
plot(diff_accurate, '--rs', 'MarkerSize', 10, 'LineWidth', 4, 'MarkerEdgeColor','b');
legend('Апостериорная оценка', 'Фактическая точность')
  

% РАССЧЕТЫ-----------------------------------------------------------------------------------------

function [x,  energy_corrected] = dot_scheme(net, u, N, energy, d, eps)

% первая итерация
[a, b, c] = coeffs(net, u, N, energy);

x = progonka(a, b, c, d, N);
energy_corrected = energy + sum(x .* d)/sum(x .* x);
x = x/max(x);

% зацикливание 
while abs((energy_corrected - energy)/(energy_corrected+energy)) > eps
    
    energy = energy_corrected;
    
    d = x;
    [a, b, c] = coeffs(net, u, N, energy);
    
    x = progonka(a, b, c, d, N);
    energy_corrected = energy + sum(x .* d)/sum(x .* x);
    x = x/max(x);
    
end
end

function net = auto_mesh(N_ex, N_in)
% введите границы областей
l_ex_border = -10;
r_ex_border = 10;

l_in_border = -1;
r_in_border = 1;

% подсчет шага в центральной части в центральной части
step = (r_in_border - l_in_border)/N_in;

% рассчет внутренней вспомогательной сетки
in_net = [];
for n = 1:1:N_in-1
    in_net(n) = l_in_border + n*step;
end

% неравномерная сетка
net = [linspace(l_ex_border, l_in_border, N_ex), in_net, linspace(r_in_border, r_ex_border, N_ex)];

% равномерная сетка (шаг задаем по значению N_in)
% h1 = abs((l_ex_border- l_in_border))/N_ex;
% h2 = abs((l_in_border - r_in_border))/N_in;
% h3 = abs((r_in_border - r_ex_border))/N_ex;
% 
% net1 = l_ex_border:h1:l_in_border;
% net2 = l_in_border + h2 : h2 : r_in_border;
% net3 = r_in_border + h3 : h3 : r_ex_border;
% 
% net = [net1, net2, net3];

end

function [h, u] = count_h(net, N, v, N_ex, N_in)
h = [];
u = [];

for n = 1:1:N+1
    h(n) = abs(net(n+1) - net(n));

%     потенциальная яма конечной глубины
    if n <= (N_ex-1) | n >= (N_ex+N_in)
        u(n) = v;
    end
    if n >  (N_ex-1) && n < (N_ex+N_in)
        u(n) = 0;
    end

%     потенциал вида x^2
%     u(n) = net(n)^2;

end
% figure;
% plot(u, '-k.', 'MarkerSize', 30);

end

function [a, b, c] = coeffs(net, u, N, energy)
a = zeros(1, N);
b = zeros(1, N);
c = zeros(1, N);

for n = 2:1:N+1
    c(n-1) = 1/(net(n+1)- net(n-1)) * (1/(net(n+1)- net(n)) + 1/(net(n) - net(n-1))) + u(n) - energy;
end

for n = 2:1:N
    b(n-1) = 1/(net(n+1) - net(n-1)) * 1/(net(n+1) - net(n));
end

for n = 3:1:N+1
    a(n-1) = 1/(net(n+1) - net(n-1)) * 1/(net(n)- net(n-1));
end

end

function x = progonka(a, b, c, d, N)

x = zeros(1, N);
alpha = zeros(1, N+1);
betha = zeros(1, N+1);

for n = 1:1:N
    alpha(n+1) = b(n)/(c(n)-a(n)*alpha(n));
    betha(n+1) = (a(n)*betha(n) + d(n))/(c(n) - a(n)*alpha(n));
end

x(N) = betha(N+1);
for n = N-1:-1:1
    x(n) = x(n+1)*alpha(n+1) + betha(n+1);
end

end

