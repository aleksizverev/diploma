% clear;
% format long
% E =bisec(12, 14, 0.0000001);
% disp(E)
% 
% gr = []
% for E = 1:1:50
%     gr(E) = f(E, 35, 2)
% end
% figure;
% hold on;
% grid on;
% plot(gr, '--g+','MarkerSize', 20, 'LineWidth', 4, 'MarkerEdgeColor','b');

function E = bisec(a, b, eps, v_0, width)

while b - a > eps
    
    E = (a+b)/2;
    
    if f(E, v_0, width) == 0
        break
    end
 
    if f(a, v_0, width)*f(E, v_0, width) <= 0
        b = E;
    else
        
        if f(b, v_0, width)*f(E, v_0, width) <= 0
            a = E;
        end
     
    end
end

end

function res = f(E, v_0, width)
    res = v_0*cos(width*sqrt(2*E)) - 2*E + v_0;
end 